package cordova.plugin.utilsandroid;

import android.content.pm.PackageManager;
import android.app.Activity;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import cordova.plugin.utilsandroid.AsyncPrintTicket;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Build;
import android.content.Context;
import java.util.Arrays;
import android.net.ConnectivityManager;
import android.view.WindowManager;
import android.net.NetworkInfo;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Intent;

import android.content.SharedPreferences;

import android.preference.PreferenceManager;
import java.io.DataOutputStream;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;

import android.provider.AlarmClock;
import java.io.FileReader;
import java.io.File;
import android.net.Uri;
import android.os.Environment;
//import android.support.v4.content.FileProvider;

import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.MotionEvent;
import android.view.Gravity;
//import com.iurban.demomenu.R;
import android.graphics.PixelFormat;
import android.graphics.Color;
import java.io.BufferedReader;

import android.os.PowerManager;

import android.content.ContentResolver;
import android.provider.Settings;
/*import android.support.v4.content.ContextCompat;
import android.Manifest;*/

public class UtilsAndroid extends CordovaPlugin {
    private String TAG = "HOTEL_DIGITAL";
    private Activity activity;
    private Context context;
    int counter = 0, timeout = 10;
    static WindowManager manager;
    public static customViewGroup notif_view;

    //public static final String EXIT_KIOSK = "exitKiosk";    
    //public static final String IS_IN_KIOSK = "isInKiosk";
    //private static final String PREF_KIOSK_MODE = "pref_kiosk_mode";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        context = this.cordova.getActivity().getApplicationContext();
        activity = this.cordova.getActivity();



        if (action.equals("printTicket")) {
            this.printTicket(args, callbackContext);
            return true;
        } else if (action.equals("getMacAddress")) {
            this.getMacAddress(callbackContext);
            return true;
        } else if (action.equals("connectToWifi")) {
            this.connectToWifi(args, callbackContext);
            return true;
        } else if (action.equals("enableWifi")) {
            this.enableWifi(callbackContext);
            return true;
        } else if (action.equals("log")) {
            this.log(args,callbackContext);
            return true;
        } else if (action.equals("logError")) {
            this.logError(args,callbackContext);
            return true;
        } else if (action.equals("setDeviceOwner")) {
            this.setDeviceOwner(callbackContext);
            return true;
        } else if (action.equals("enableKioskMode")) {
            this.enableKioskMode(callbackContext);
            return true;
        } else if (action.equals("removeDeviceOwner")) {
            this.removeDeviceOwner(callbackContext);
            return true;
        } else if (action.equals("chooseLauncher")) { 
            PackageManager packageManager = context.getPackageManager();
            packageManager.clearPackagePreferredActivities(context.getPackageName());

            Log.d(TAG, "launchAppChooser");
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            this.cordova.getActivity().startActivity(Intent.createChooser(intent, "Elija launcher"));
            this.cordova.getActivity().finish();
            return true;
        } else if (action.equals("openAlarmManager")) {   
            this.openAlarmManager(callbackContext);
             return true;
        } else if (action.equals("isPackageInstalled")) {
            this.isPackageInstalled(args, callbackContext);
            return true;
        } else if (action.equals("launchAppInstalled")) {
            this.launchAppInstalled(args, callbackContext);
            return true;
        } else if (action.equals("screenON")) {
            this.screenON(callbackContext);
            return true;
        } else if (action.equals("installPackageSilently")) {
            this.installPackageSilently(args, callbackContext);
            return true;
        } else if (action.equals("installPackage")) {
            this.installPackage(args, callbackContext);
            return true;
        } else if (action.equals("enablePullNotificationTouch")) {
            this.enablePullNotificationTouch(callbackContext);
            return true;
        } else if (action.equals("disablePullNotificationTouch")) {
            this.disablePullNotificationTouch(callbackContext);
            return true;
        } else if (action.equals("readFile")) {
            this.readFile(args, callbackContext);
            return true;
        } else if (action.equals("wakeLockAcquire")) {
            this.wakeLockAcquire(callbackContext);
            return true;
        } else if (action.equals("wakeLockRelease")) {
            this.wakeLockRelease(callbackContext);
            return true;
        } else if (action.equals("requestWriteSettingsPermissions")) {
            this.requestWriteSettingsPermissions(callbackContext);
            return true;
        } else if (action.equals("setBrightness")) {
            this.setBrightness(args, callbackContext);
            return true;
        } else if (action.equals("finish")) {
            this.finish(callbackContext);
            return true;
        }        
        return false;
    }

    private Class getMainActivity(){
        Class mainActivity = null;
        PackageManager pm = context.getPackageManager();

        Log.d(TAG,"packagemanager " + pm);

        Intent launchIntent = pm.getLaunchIntentForPackage(context.getPackageName());

        Log.d(TAG,"launchintent " + launchIntent + " package " + context.getPackageName());

        try {
            //mainActivity = Class.forName(launchIntent.getComponent().getClassName());
            mainActivity = Class.forName(context.getPackageName()+".MainActivity");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error "+e);
        }

        return mainActivity;
    }

      private void chooseLauncher(CallbackContext callback){
            PackageManager packageManager = context.getPackageManager();
            packageManager.clearPackagePreferredActivities(context.getPackageName());
            
            ComponentName componentName = new ComponentName(context, getMainActivity());
            packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

            Log.d(TAG, "in choose launcher");
            Intent selector = new Intent(Intent.ACTION_MAIN);
            selector.addCategory(Intent.CATEGORY_HOME);
            //Intent chooser = Intent.createChooser(intent, "Select destination...");
            //if (intent.resolveActivity(cordova.getActivity().getPackageManager()) != null) {
            Log.d(TAG, "1 starting choose launcher...");
            selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cordova.getActivity().startActivity(selector);
            //} 
    }

    private void printTicket(JSONArray args, CallbackContext callback){
        if(args != null){
            try{
                Log.d(TAG, "2 in print ticket");               
                
                String productsStr = args.getJSONObject(0).getString("products");
                // Log.d(TAG,"productsStr " + productsStr);

                JSONArray products = new JSONArray(productsStr); 
                Log.d(TAG,"jsonArray " + products);

                /*String productName = products.getJSONObject(0).getString("name");
                Log.d(TAG, "product name " + productName);

                Log.d(TAG,"message " + args.getJSONObject(0).getString("message"));*/

                AsyncPrintTicket ticketPrinter = new AsyncPrintTicket(context, 
                args.getJSONObject(0).getString("message"),
                args.getJSONObject(0).getString("location"),
                products,
                args.getJSONObject(0).getString("ip"), 
                args.getJSONObject(0).getString("port"));

                ticketPrinter.execute();

                callback.success("Ticket enviado a impresora"); 
            }catch(Exception e){
                callback.error("Something went wrong " + e);
            }
        }else{
            callback.error("Please donot pass null value");
        }
    }

    public void getMacAddress(CallbackContext callback) {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                      callback.success("02:00:00:00:00:00"); 
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:",b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }

                //return res1.toString();
                callback.success(res1.toString()); 
            }
        } catch (Exception ex) {
             callback.success("02:00:00:00:00:00");
        }        
    }

    public void connectToWifi(JSONArray args, CallbackContext callback) {
         try {            
            WifiManager wifiManager = (WifiManager) this.cordova.getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            String networkSSID = args.getJSONObject(0).getString("ssid");
            String password = args.getJSONObject(0).getString("password");
            timeout = Integer.valueOf(args.getJSONObject(0).getString("timeout"));

            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + networkSSID + "\"";

            conf.preSharedKey = "\"" + password + "\"";

            conf.status = WifiConfiguration.Status.ENABLED;
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

            int networkId = wifiManager.getConnectionInfo().getNetworkId();
            Log.d(TAG, "network disabled " + wifiManager.disableNetwork(networkId));
            Log.d(TAG, "network removed " + wifiManager.removeNetwork(networkId));
            Log.d(TAG, "network saved " + wifiManager.saveConfiguration());

            wifiManager.addNetwork(conf);

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                    wifiManager.disconnect();

                    wifiManager.enableNetwork(i.networkId, true);
                    wifiManager.reconnect();
                    Log.d(TAG, i.SSID + " " + conf.preSharedKey);
                    break;
                }
            }

            verifyingConnection(callback);

            //WiFi Connection success, return true
           
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            callback.error("" + e);
            //return false;
        }
    }

    public void verifyingConnection(CallbackContext callback){
        counter = 0;
        final Handler handler = new Handler();    
        handler.postDelayed(new Runnable() {
            public void run() {
                //Log.d(CustomConstants.TAG, "CONNECTED " + InternetHelper.wifiIsConnected(context));
                if (!wifiIsConnected()) {
                     counter++;
                    if(counter < timeout) {
                        Log.d(TAG, "checking connection, counter " + counter);
                        handler.postDelayed(this, 1000);                       
                    }else{
                         callback.error("timeout");
                    }
                } else {
                        callback.success("true"); 
                }
            }
        }, 1000);
    }

    public boolean wifiIsConnected(){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public void enableWifi(CallbackContext callback) {
        try{
            boolean wifiEnabled = false;
            WifiManager wifiManager = (WifiManager) this.cordova.getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (!wifiManager.isWifiEnabled()) {
                wifiEnabled = wifiManager.setWifiEnabled(true);
            }
            callback.success(""+wifiEnabled); 
        }catch(Exception e){
             callback.error("" + e);
        }
    }

    public void log(JSONArray args, CallbackContext callback) {
        try{
            TAG = args.getJSONObject(0).getString("TAG");

            Log.d(TAG, args.getJSONObject(0).getString("message"));
        }catch(Exception e){
            Log.e(TAG, "" + e);
        }
    }

    public void logError(JSONArray args, CallbackContext callback){
        try{
            TAG = args.getJSONObject(0).getString("TAG");

            Log.e(TAG, args.getJSONObject(0).getString("message"));
        }catch(Exception e){
            Log.e(TAG, "" + e);
        }
    }

    public void setDeviceOwner(CallbackContext callback) {
       
            try{
                Log.d(TAG, "in device owner " );
                Process su = Runtime.getRuntime().exec("su");
                DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

                outputStream.writeBytes("dpm set-device-owner " + this.cordova.getActivity().getPackageName() + "/" + this.cordova.getActivity().getPackageName() + ".MyAdmin\n");
                outputStream.flush();

                outputStream.writeBytes("exit\n");
                outputStream.flush();
                su.waitFor();              
            
            }catch(Exception e){
                Log.e(TAG,"error "+e);
            }
         
        
    }

    public void enableKioskMode(CallbackContext callback) {
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) this.cordova.getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
            
            try{
                Log.d(TAG,"enabling kiosk mode" );
                ComponentName mDPM = new ComponentName("cordova.plugin.utilsandroid", "cordova.plugin.utilsandroid.MyAdmin.MyAdmin");
                
                
            //if (mDevicePolicyManager != null && mDevicePolicyManager.isDeviceOwnerApp(this.cordova.getActivity().getPackageName())) {
                String[] packages = {this.cordova.getActivity().getPackageName()};
                mDevicePolicyManager.setLockTaskPackages(mDPM, packages);
               
                    Log.d(TAG,"in start locktask");
                    this.cordova.getActivity().startLockTask();
               
            //}
            
             /*else {
                // Log.d(CustomConstants.TAG,"DISABLE "+(activity instanceof LicenseManager));

                //if (disable_notif)
                //    Utils.disablePullNotificationTouch(activity);

                // if(activity instanceof MainActivity)
                //setAsLauncher(activity);
            }*/
               

            }catch(Exception e){
                Log.e(TAG,"error "+e);
            }

        //}
    }

    public void removeDeviceOwner(CallbackContext callback) {        
        try {
            Log.d(TAG, "in removeDeviceOwner");
            DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) this.cordova.getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
            mDevicePolicyManager.clearDeviceOwnerApp(this.cordova.getActivity().getPackageName());
            Log.d(TAG, "Device owner removed!");
        } catch (SecurityException se) {
            Log.e(TAG, se.toString());
        }
    }

    public void openAlarmManager(CallbackContext callback) {
        Log.d(TAG, "in open alarm manager");

        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
        this.cordova.getActivity().startActivity(Intent.createChooser(i, "Set alarm"));
    }

    public boolean isPackageInstalled(JSONArray args, CallbackContext callback) {
        boolean isInstalled;

        Log.d(TAG, "in package installed ");
 
        try {
            String packageName = args.getJSONObject(0).getString("package");    

            Log.d(TAG, "package " + packageName + " not installed");
            context.getPackageManager().getPackageInfo(packageName, 0);
            isInstalled = true;
        } catch (Exception e) {
            isInstalled = false;
            Log.e(TAG, e.toString() );
        }
        return isInstalled;
    }

    public void launchAppInstalled(JSONArray args, CallbackContext callback) {
        Log.d(TAG, "in launch app installed ");

         if(args != null){
            try{                
                String packageName = args.getJSONObject(0).getString("package");               

                Log.d(TAG, "launching package " + packageName);

                // If package doesn't exist crash the function
                context.getPackageManager().getPackageInfo(packageName, 0);

                Intent superIntent = this.cordova.getActivity().getPackageManager().getLaunchIntentForPackage(packageName);      
                this.cordova.getActivity().startActivity(superIntent);                   

                this.cordova.getActivity().finish();
            } catch (Exception e) {
                callback.error("App is not installed");
                Log.e(TAG, e.toString() );
            }              
        } 
    }

    public void screenON(CallbackContext callback){
        this.cordova.getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void installPackageSilently(JSONArray args, CallbackContext callback) {
        String SD_DOWNLOADS = Environment.getExternalStorageDirectory().getAbsolutePath().concat("/Download");

        try {
            String appName = args.getJSONObject(0).getString("appName");    
            String appServerVersion = args.getJSONObject(0).getString("appServerVersion"); 
            String path = SD_DOWNLOADS + "/" + appName + " " + appServerVersion + ".apk";

            Log.d(TAG, "Installing app silently " + path);

            File file = new File(path);
            if (file.exists()) {            
                Process process = Runtime.getRuntime().exec("su -c pm install -r -d " + SD_DOWNLOADS + "/" + appName + " " + appServerVersion + ".apk root");
                int result = process.waitFor();
                Log.d(TAG, "Installed result " + result);
         
            }
        } catch (Exception e) {
                callback.error(e.toString() );
                e.printStackTrace();
                Log.e(TAG, "");
        }
    }

    public void installPackage(JSONArray args, CallbackContext callback) {
        String SD_DOWNLOADS = Environment.getExternalStorageDirectory().getAbsolutePath().concat("/Download");

        Intent intent = null;
        try {
            String appName = args.getJSONObject(0).getString("appName");    
            String appServerVersion = args.getJSONObject(0).getString("appServerVersion"); 
            String path = SD_DOWNLOADS + "/" + appName + " " + appServerVersion + ".apk";

            Log.d(TAG, "Installing app " + path);
            
            Uri fileUri;
            File file = new File(path, appName + " " + appServerVersion + ".apk");
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                fileUri = android.support.v4.content.FileProvider.getUriForFile(context, this.cordova.getActivity().getPackageName() + ".external_files", file);
                intent.setData(fileUri);
            } else {*/
                intent = new Intent("android.intent.action.VIEW");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                fileUri = Uri.fromFile(file);
                intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                
            //}

            context.startActivity(intent);
            this.cordova.getActivity().finish();

        } catch (Exception e) {
            callback.error( e.toString() );
            e.printStackTrace();
            Log.e(TAG, "");
        }       
    }

    public void enablePullNotificationTouch(CallbackContext callback) {
        Log.d(TAG, "enabling notif");
        if (notif_view != null)
            notif_view.setVisibility(View.GONE);
    }

    public void disablePullNotificationTouch(CallbackContext callback) {
        Log.d(TAG, "disabling notif");
        manager = ((WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE));  

        WindowManager.LayoutParams localLayoutParams2 = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            localLayoutParams2.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else{
            localLayoutParams2.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        }
        //ADDING bar top
        localLayoutParams2.gravity = Gravity.TOP;
        localLayoutParams2.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        localLayoutParams2.width = WindowManager.LayoutParams.MATCH_PARENT;
        //localLayoutParams2.height = (int) context.getResources().getDimension(R.dimen.stat_height);
        localLayoutParams2.height = 40;
        localLayoutParams2.format = PixelFormat.RGBX_8888;

        if (notif_view == null) {
            notif_view = new customViewGroup(context);
            notif_view.setBackgroundColor(Color.BLACK);
            manager.addView(notif_view, localLayoutParams2);
        } else {
            notif_view.setVisibility(View.VISIBLE);
        }
    }

     //Add this class in your project
    public static class customViewGroup extends ViewGroup {
        public customViewGroup(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            return true;
        }
    }

    public void readFile(JSONArray args, CallbackContext callback){
        try {
            File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);                
     
            File file = new File(downloads,args.getJSONObject(0).getString("file"));

            StringBuilder text = new StringBuilder();
        
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            callback.success(text.toString()); 
        }
        catch (Exception e) {
            callback.error( e.toString() );
        }
    }

    public void wakeLockAcquire(CallbackContext callback){
        Log.d(TAG,"in wakeLock acquire");
        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::"+TAG);
        wakeLock.acquire();
    }

     public void wakeLockRelease(CallbackContext callback){
        Log.d(TAG,"in wakeLock release");
        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp::"+TAG);
        wakeLock.release();
    }

    public void requestWriteSettingsPermissions(CallbackContext callback){
        Log.d(TAG,"requesting write settings...");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean canWrite = Settings.System.canWrite(activity);
            Log.d(TAG,"can write settings..." + canWrite);
            if (!canWrite) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                activity.startActivity(intent);      
                callback.error( "can't write settings" );          
            }else{
                callback.success("can write settings"); 
            }
        }else{
            callback.success("no need write settings permission"); 
        }
    }

    public void setBrightness(JSONArray args, CallbackContext callback){
        try {
            String brightness = args.getJSONObject(0).getString("brightness");
            int parseBrightness = Math.round(Integer.parseInt(brightness)*255);

            if(parseBrightness < 0)
                parseBrightness = 0;
            else if(parseBrightness > 1)
                parseBrightness = 255;

            ContentResolver cResolver = context.getContentResolver();
            Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, parseBrightness);
        }catch (Exception e) {
            callback.error( e.toString() );
        }
    }

    public void finish(CallbackContext callback){
        Log.d(TAG,"in finish");
        this.cordova.getActivity().finish();
    }
}