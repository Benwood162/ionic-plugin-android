var exec = require('cordova/exec');

module.exports.printTicket = function (arg0,success, error){
    exec(success, error, 'UtilsAndroid', 'printTicket', [arg0]);
}

module.exports.getMacAddress = function (success, error){
    exec(success, error, 'UtilsAndroid', 'getMacAddress');
}

module.exports.connectToWifi = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'connectToWifi', [arg0]);
}

module.exports.enableWifi = function (success, error){
    exec(success, error, 'UtilsAndroid', 'enableWifi');
}

module.exports.log = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'log', [arg0]);
}

module.exports.logError = function (arg0, success, error){
    exec(success, error,'UtilsAndroid', 'logError', [arg0]);
}

module.exports.chooseLauncher = function (success, error){
    exec(success, error, 'UtilsAndroid', 'chooseLauncher');
}

module.exports.enableKioskMode = function (success, error){
    exec(success, error, 'UtilsAndroid', 'enableKioskMode');
}

module.exports.setDeviceOwner = function (success, error){
    exec(success, error, 'UtilsAndroid', 'setDeviceOwner');
}

module.exports.removeDeviceOwner = function (success, error){
    exec(success, error, 'UtilsAndroid', 'removeDeviceOwner');
}

module.exports.openAlarmManager = function (success, error){
    exec(success, error, 'UtilsAndroid', 'openAlarmManager');
}

module.exports.isPackageInstalled = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'isPackageInstalled', [arg0]);
}

module.exports.launchAppInstalled = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'launchAppInstalled', [arg0]);
}

module.exports.installPackageSilently = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'installPackageSilently', [arg0]);
}

module.exports.installPackage = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'installPackage', [arg0]);
}

module.exports.screenON = function (success, error){
    exec(success, error, 'UtilsAndroid', 'screenON');
}

module.exports.enablePullNotificationTouch = function ( success, error){
    exec(success, error, 'UtilsAndroid', 'enablePullNotificationTouch');
}

module.exports.disablePullNotificationTouch = function ( success, error){
    exec(success, error, 'UtilsAndroid', 'disablePullNotificationTouch');
}

module.exports.readFile = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'readFile', [arg0]);
}

module.exports.wakeLockAcquire = function (success, error){
    exec(success, error, 'UtilsAndroid', 'wakeLockAcquire');
}

module.exports.wakeLockRelease = function (success, error){
    exec(success, error, 'UtilsAndroid', 'wakeLockRelease');
}

module.exports.requestWriteSettingsPermissions = function (success, error){
    exec(success, error, 'UtilsAndroid', 'requestWriteSettingsPermissions');
}

module.exports.setBrightness = function (arg0, success, error){
    exec(success, error, 'UtilsAndroid', 'setBrightness', [arg0]);
}

module.exports.finish = function (success, error){
    exec(success, error, 'UtilsAndroid', 'finish');
}

